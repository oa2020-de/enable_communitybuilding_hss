# Über

Die Plattform ENABLE!– Bibliotheken, Verlage und Autor_innen für Open Access in den Geistes- und Sozialwissenschaften dient dazu, die Herausforderungen und Chancen, die Open Access allen Beteiligten des wissenschaftlichen Publikationsprozesses bietet, in Form eines Community-Building-Prozesses aufzugreifen. Ziel ist es, aus den einzelnen Akteuren – den Bibliotheken, Verlagen, Intermediären, Händlern und Autor_innen – ein partnerschaftliches Netzwerk zum gemeinsamen Ermöglichen von Open-Access-Publikationen und zur Entwicklung neuer, innovativer Modelle zu bilden.

Dieses Git-Repository dient ausschließlich als File-Server. Hier haben Sie als Verlag die Möglichkeit, eine leere csv-Tabelle herunterzuladen, sie mit Publikationen
aus kooperativen Open-Access-Projekten zu füllen und dann uns für den Import in die ENABLE-Plattform (per Mail an info.enable-oa@uni-bielefeld.de) zu schicken. 

Eine direkte Export/Import-Lösung per Schnittstelle ist in Arbeit.


### Was muss ich beachten?

Für die csv-Tabelle mit den Angaben zu den Publikationen gilt folgende Mandatregel für das Ausfüllen der Felder:

**Obligatorisch**
*  Typ
*  Titel
*  Autor_innen bzw. Herausgeber_innen
*  Link zum Volltext
*  Zuordnung zu einer Fachdisziplin/ einem Programmbereich
*  Cover (als zip-Ordner mitschicken oder URL in die Tabelle eintragen)
*  Erscheinungjahr
*  Creative-Commons-Lizenz

**Fakultativ**
*  Abstract
*  Untertitel (falls vorhanden)
*  Zuordnung zu einem Open-Access-Projekt (OGeSoMo, KU Select, transcript Open Library etc.)
*  ISBN
*  ISSN
*  DOI



### Cover

Die Cover können Sie entweder per URL-Verlinkung in die csv-Tabelle integrieren, oder gesammelt als ZIP-Ordner per Mail mitschicken. Bitte beachten Sie, das nur
.jpg oder .png Dateien angenommen werden. Die Dateibezeichnung sollte eindeutig sein (z.B. TiteldesBuches.jpg)

#### Kontakt

Bei Fragen, Problemen oder sonstigen Anliegen, können Sie sich jederzeit an uns wenden: info.enable-oa@uni-bielefeld.de
